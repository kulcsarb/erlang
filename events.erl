-module(events).

-export([new/0, stop/1, add/2, count/1, list/1, event_service/0]).

-compile([
		{nowarn_unused_function, [event_service/1, handle_unknown_message/1, count_events/1, list_events/1, print_events/1]}
		]).

-include("events.hrl").

-define(SERVICE_NAME, event_service).



new() ->
	Pid = spawn(?MODULE, event_service, []),
	register(?SERVICE_NAME, Pid),
	global:register_name(?SERVICE_NAME, Pid),
	Pid.

stop(Pid) when is_pid(Pid) ->
	exit(Pid, shutdown).

add(Pid, Event) when is_pid(Pid) ->
	Pid ! {self(), event, Event},
	ok.

count(Pid) when is_pid(Pid) ->
	Self = self(),
	Node = node(),
	Pid ! {Self, count},			
	receive
		{?SERVICE_NAME, Node, {count, Count}} -> Count;
		_ -> 0
	end.

list(Pid) when is_pid(Pid) ->
	Pid ! {self(), list},
	ok.


event_service() ->
	process_flag(trap_exit, true),
	try 
		event_service([]) 
	catch
		exit:Exit ->
			io:format("EXIT was called! reason: ~p~n", [Exit])
	after
		io:format('END.~n')
	end.
	

event_service(Events) -> 
	try message_loop(Events) of 
		{reply, {To, Reply}, NewState} -> 
			To ! {?SERVICE_NAME, node(), Reply},
			event_service(NewState);
		{noreply, NewState} ->
			event_service(NewState)
	catch 
		error:Error ->
			io:format("Error happened! ~p~n", [Error]),
			event_service(Events);			
		exit:fuck ->
			io:format("FUCKing errors! ~n"),
			event_service(Events)
	end.

message_loop(Events) ->
	receive
		{_From, event, Event} -> 		
			io:format("Adding event: ~p~n", [length([Event | Events])]),
			{noreply, [Event | Events]};			
		{From, count} -> 
			Count = count_events(Events),
			{reply, {From, {count, Count}}, Events};
		{_From, list} -> 
			list_events(Events),	
			{noreply, Events};
		fuck -> 
			erlang:exit(fuck);
		oops ->
			erlang:error(oooops);
		{'EXIT', _, Reason} ->
			io:format("EXIT message received!  ~p~n", [Reason]),
			erlang:exit(Reason);			
		Message -> 	handle_unknown_message(Message), 
					{noreply, Events}					
	end.	



handle_unknown_message(Message) -> 
	io:format("Unknown message: ~p~n", [Message]).

count_events(Events) -> 
	Count = length(Events),
	io:format("# of events: ~p~n", [Count]),
	Count.

list_events(Events) -> 	
	print_events(Events).

print_events([]) -> 
	io:format("No events");

print_events(T) when is_list(T) ->
	io:format("Stored events:"),
	lists:map(fun(E) -> io:format("~p~n", [E]) end, T).



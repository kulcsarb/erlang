-module(pinger).

-behaviour(gen_server).

%% Client
-export([start_link/0, ping/0, pang/0]).

%% API
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(counter, {ping=0, pong=0}).

-define(SERVER, ?MODULE).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%ping() ->
%    gen_server:call(?MODULE, ping).
%
%pong() ->
%    gen_server:call(?MODULE, pong).

ping() ->
    gen_server:cast(?MODULE, ping).

pang() ->
    gen_server:cast(?MODULE, pang).

%%--------------------------------
%% @doc API implementation for gen_server
%% @end
%%---------------------------------

init([]) ->
    io:format("PINGER started! ~p~n", [self()]),
    {ok, #counter{}}.

handle_call(Request, _, #counter{ping=PingCounter, pong=PongCounter}) ->
    case Request of
        ping -> io:format("PING! ~p~n", [PingCounter]),
                {reply, {ok, PingCounter+1}, #counter{ping=PingCounter+1, pong=PongCounter}};
        pong -> io:format("PONG! ~p~n", [PongCounter]),
                {reply, {ok, PongCounter+1}, #counter{ping=PingCounter, pong=PongCounter+1}}
    end.

handle_cast(ping, State) ->
    io:format("PING! ~p~n", [self()]),
    {noreply, State}.

handle_info(Info, State) ->
    io:format("handle_info: ~p", [Info]),
    {noreply, State}.

terminate(Reason, _) ->
    io:format("PINGER terminate:~n").

code_change(_, State, _) ->
    {ok, State}.


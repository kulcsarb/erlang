-module(counter_app).

-behavior(application).

-export([start/2, stop/1]).


start(_StartType, _StartArgs) ->
	io:format("Application starts~n", []),
	counter_sup:start_link().


stop(State) ->
	io:format("Application is stopped ~p~n", [State]).
-module(counter_sup).


-behavior(supervisor).

-export([start_link/0, init/1]).

-define(SERVER, ?MODULE).


start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).


init([]) ->
	io:format("Supervisor starts~n", []),
	Supervisor = #{strategy=> one_for_one, intensity=>10, period=>60},
	Children = [#{id=>counter_id
				, start => {counter, start_link, []}
				, restart => permanent
				, shutdown => brutal_kill
				, type=> worker
				, module=> [counter]}],
	{ok, {Supervisor, Children}}.
	



%% my very first erlang module

-module(my).

-export([pie/0, sum/1, tail_sum/1, list_sum/1, tail_list_sum/1, max/1, max_case/1, max_if/1, reverse/1, tail_reverse/1]).

-export([cartesian/1]).

pie() ->
 "RECOMPILED!!!! 3.14".


% practicing recursion with sum
% this is a body recursive implementation of sum, because the last 
% operation in sum(N) is + 
sum(0) -> 0;
% note that here the function call itself act as a temporary 'variable'
sum(N) -> N + sum(N-1).


%% Sum using tail optimized recursion
tail_sum(N) ->
    do_sum(N, 0).

% tail optimization works like introducing a designated accumulator variable to allow the function call to be the last one
do_sum(N, Total) when N == 0 ->
    Total;
do_sum(N, Total) -> do_sum(N-1, Total+N).


% anothet body recursive function, to sum up elements of a list
list_sum([]) -> 0;
list_sum([Head | Tail]) ->
    Head + list_sum(Tail).

% same function, implemented with an accumulator to allow tail rec. optimization
tail_list_sum(List) ->
    tail_list_sum(List, 0).
tail_list_sum([], Sum) -> Sum;
tail_list_sum([Head | Tail], Sum) -> tail_list_sum(Tail, Sum + Head).


% find the max of a list, using guards, pattern matching, and tail rec.
max(List) -> do_max(List, 0).

do_max([], Max) -> Max;
do_max([H | T], Max) when H > Max -> do_max(T, H);
do_max([_ | T], Max) -> do_max(T, Max).


% I can do this with case:
max_case(List) -> max_case(List, 0).
max_case(List, Max) ->
    case List of
        [] -> Max;
        [H | T] when H > Max -> max_case(T, H);
        [_ | T] -> max_case(T, Max)
    end.


% and I can do it with if - if is basicly case without pattern matching, using guards only
max_if(List) -> max_if(List, 0).
max_if([], Max) -> Max;
max_if([H | T], Max) ->
    if
        H > Max -> max_if(T, H);
        true -> max_if(T, Max)
    end.


% reversing using body recursion
reverse([]) -> [];
reverse([H | T]) -> reverse(T) ++ [H | []].

% - hopefully, a  tail rec. version of reverse....
tail_reverse(List) -> do_reverse(List, []).
do_reverse([], R) -> R;
do_reverse([H | T], R) -> do_reverse(T, [H | []] ++ R).



cartesian(L) ->
    lists:map(fun(X) -> lists:flatten(X) end, iter_cartesian(L)).

iter_cartesian([]) -> [];
iter_cartesian([L]) -> L;
iter_cartesian([F, N|T]) -> 
    iter_cartesian([ cartesian_pair(F, N) | T]).

cartesian_pair(L1, L2) ->
    [[X, Y] || X <- L1, Y <- L2].

%%%--------------------------------------
%% @doc a module to play with server processes
%% @end
%%%--------------------------------------

-module(server).

-export([simple_ping/0, process_ping/0, ping_forever/0, ping_forever/1, ping_counted/0]).

-record(counter, {ping=0, pong=0}).

simple_ping() ->
    io:fwrite("ping() -> pong! ~p~n", [self()]),
    {ok, pong}.


%% @doc start it with pid = spawn(server, process_ping, []).
process_ping() ->
    io:fwrite("started: ~p~n", [self()]),
    receive
        ping -> io:fwrite("ping() -> 1. pong!~n", [])
    end,
    io:format("after 1., waiting for 2. msg~n"),
    receive
        ping -> io:fwrite("ping() -> 2. pong!~n", [])
    end,
    io:format("end of ~p .~n", [self()]).


ping_forever() ->
    ping_forever(0).

%% @doc start it with pid = spawn(server, process_ping, [10]).
ping_forever(Counter) when is_integer(Counter) ->
    io:fwrite("~p is waiting....~n", [self()]),
    receive
        ping -> io:fwrite("ping() -> ~p. pong!~n", [Counter])
    end,
    ping_forever(Counter+1).



ping_counted() ->
    ping_counted(#counter{ping=0, pong=0}).

%% @doc start it with pid = spawn(server, process_ping, [10]).
ping_counted(#counter{ping=PingCount, pong=PongCount}) ->
    io:fwrite("~p is waiting....~n", [self()]),
    receive
        ping -> io:fwrite("ping() -> ~p. ~n", [PingCount]),
                ping_counted(#counter{ping=PingCount+1, pong=PongCount});
        pong -> io:fwrite("pong() -> ~p. ~n", [PongCount]),
                ping_counted(#counter{ping=PingCount, pong=PongCount+1});
        stop -> {}
    end,
    io:fwrite("~p stopped.~n", [self()]).



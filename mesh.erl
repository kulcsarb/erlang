-module(mesh).
-include_lib("eunit/include/eunit.hrl").

-export([start/0]).


start() -> 
	connect(node()).	


connect(nonode@nohost) ->
	io:format("node name is not specified! ~p~n", [node()]),
	{error, noname};


connect(Node) ->
	case split_node_name(Node) of
		{error, wrong_sname_format} -> 
				io:format("-sname must get a name in <clusername><nodenum> format!!"),
				error;
		{ClusterName, NodeNum, Host} -> 		
				io:format("Connecting to cluster ~p...~n", [ClusterName]),		
				connect_cluster(ClusterName, Host, NodeNum-1),
				ok
	end.	


connect_cluster(ClusterName, Host) ->
	connect_cluster(ClusterName, Host, 1).

connect_cluster(_, _, NodeNum) when NodeNum < 0 ->
	true;


connect_cluster(ClusterName, Host, NodeNum) ->	
	NodeName = assemble_node_name(ClusterName, NodeNum, Host),
	case connect_node(NodeName) of
		false -> connect_cluster(ClusterName, Host, NodeNum-1);
		_ -> true
	end.


connect_node(NodeName) when NodeName == node() -> 
	io:format("There's no point in connecting to self...~n"),
	true;


connect_node(NodeName) ->	
	io:format("Connecting to node ~p... ", [NodeName]),
	case net_kernel:connect_node(NodeName) of 
		true -> 
			io:format("connected! ~n"),
			io:format("Nodes in cluster: ~p~n", [nodes()]),
			true;
		false ->
			io:format("failed! ~n"),
			false
	end.


assemble_node_name(ClusterName, NodeNum, Host) -> 
	list_to_atom(ClusterName ++ integer_to_list(NodeNum) ++ "@" ++ Host).



split_node_name(Node) ->
	Options = [{capture, all_but_first, binary}],
	Re = "^(\\D+)(\\d+)@(\\w+)",
	case re:run(atom_to_binary(Node, latin1), Re, Options) of
		{match, [ClusterName, NodeNumber, Host]} -> 
				{binary_to_list(ClusterName), binary_to_integer(NodeNumber), binary_to_list(Host)};
		nomatch -> 
				{error, wrong_sname_format}
	end.


connect_test() ->
	?assertEqual(connect(nonode@nohost), {error, noname}),
	?assertEqual(connect(node()), ok).


assemble_node_name_test() ->
	?assertEqual(assemble_node_name("pomme", 1, "arch"), pomme1@arch).

split_node_name_test() ->
	?assertEqual(split_node_name(pomme1@arch), {"pomme", 1, "arch"}),
	?assertEqual(split_node_name(something@arch), {error, wrong_sname_format}).






-module(prag).

-export([range/1, range/2, range/3, for/2, even_or_odd/1, even_or_odd_slow/1, generate_pairs/1]).

range(N) -> range(0, N).
range(Min, Max) -> range(Min, Max, 1).
range(Min, Max, Step) -> range(Min, Max, Step, []).


range(Min, Max, _, Acc) when Min == Max -> lists:reverse([Max | Acc]);
range(Min, Max, _, Acc) when Min > Max -> lists:reverse(Acc);
range(Min, Max, Step, Acc) ->
    range(Min+Step, Max, Step, [Min | Acc]).



for(Max, F) when Max > 0 -> for(0, Max, F).
for(I, Max, F) when I == Max-1 -> F(I);
for(I, Max, F) ->
    F(I),
    for(I+1, Max, F).


generate_pairs(L) ->
    pairs(L, []).

pairs([], Acc) -> Acc;
pairs([F], Acc) -> lists:reverse(Acc);
pairs([F,N|T], Acc) ->
    pairs([N|T], [{F, N} | Acc]).



even_or_odd_slow(L) ->
    {[X || X <- L, X rem 2 == 0],[X || X <- L, X rem 2 == 1] }.


even_or_odd(L) -> even_or_odd(L, [], []).

even_or_odd([], Even, Odd) ->
    {lists:reverse(Even), lists:reverse(Odd)};

even_or_odd([H|T], Even, Odd) ->
    case H rem 2 of
        1 -> even_or_odd(T, Even, [H | Odd]);
        0 -> even_or_odd(T, [H | Even], Odd)
    end.

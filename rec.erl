














	#entry{title=Title, access = Owner#user.rank, owner = Owner}.
	#user{name=Name, rank=1}.
	#user{name=Name, rank=2}.
	#user{name=Name, rank=3}.
	access_denied.
	access_denied.
	false.
	false.
	true;
	true;
	{ok, Entry#entry.content};
	{ok, Entry#entry{content=Content}};
-compile(export_all).
-module(rec).
-record(entry, {title, access=1, owner, content}).
-record(user, {name, rank=0, group=1}).
add_content(_ = #entry{}, _, _ = #user{}) ->
add_content(Entry = #entry{}, Content, User = #user{}) when Entry#entry.owner == User ->
admin(Name) -> 
can_read(_, _) ->
can_read(Entry, User) when Entry#entry.access =< User#user.rank ->
can_write(Entry = #entry{}, User = #user{}) ->
can_write(Entry = #entry{}, User = #user{}) when Entry#entry.owner == User; User#user.rank >= Entry#entry.access ->
create_doc(Title, Owner = #user{}) ->
expert(Name) ->
junior(Name) ->
read(_ = #entry{}, _ = #user{}) ->
read(Entry, User) when Entry#entry.access =< User#user.rank ->
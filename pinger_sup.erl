-module(pinger_sup).

-behaviour(gen_server).

%% Module API 
-export([start_link/0, ping/0, pang/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

%%===================================================
%%
%%  MODULE API definition
%%
%%====================================================

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

ping() ->
    gen_server:cast(?MODULE, ping).

pang() ->
    gen_server:cast(?MODULE, pang).


%%===================================================
%%
%%  gen_server callbacks 
%%
%%====================================================

init([]) ->
    io:format("SUPERVISOR: STARTING !"),
    process_flag(trap_exit, true),
    pinger:start_link(),
    {ok, {}}.

handle_call(_, _, State) ->
    {reply, {}, State}.

handle_cast(ping, State) ->
    io:format('SUPERVISOR: forwarding ping~n'),
    pinger:ping(),
    {noreply, State};

handle_cast(pang, State) ->
    io:format('SUPERVISOR: forwarding pang~n'),
    pinger:pang(),
    {noreply, State}.

handle_info({'EXIT', Pid, Reason}, State) ->
    io:format("SUPERVISOR: received EXIT info!~n"),
%    io:format("~p~n~p~n", [Pid, Reason]),
    pinger:start_link(),
    {noreply, State};

handle_info(_, State) ->
    {noreply, State}.

terminate(Reason, _) ->
    io:format("SUPERVISOR: terminate! ~n").

code_change(_, _, State) ->
    {ok, State}.




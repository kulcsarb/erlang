-module(play1).

-export([get_user/3, get_entry/2, print/1, process/1]).

-record(user, {name, email, address}).

-record(entry, {title, lang, group}).




get_user(Name, Email, Address) ->
    #user{name=Name, email=Email, address=Address}.

get_entry(Title, Language) ->
     #entry{title=Title, lang=Language}.

print(#user{name=Name, email=Email, address=_}) ->
    io:format("User: ~s (~s)~n", [Name, Email]);

print(#entry{title=T, lang=L}) ->
    io:format("Entry: ~s (~s)~n", [T, L]).


process(Parent) ->
%    io:format("Entering process: ~w, my parent is: ~w~n", [self(), Parent]),
    receive
        #user{name=Name} -> 
            io:format("User received: ~s~n", [Name]),
            process(Parent);
        #entry{title=Title} -> 
            io:format("Entry: ~s~n", [Title]),
            process(Parent);
        stop -> io:format("Exiting");
        _ -> io:format("unkown data"),
             process(Parent)
%    after 10000 ->
%      io:format("Timout!!!")
    end.




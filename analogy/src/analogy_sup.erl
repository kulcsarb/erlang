%%%-------------------------------------------------------------------
%% @doc analogy top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(analogy_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    SOffice = {analogy_soffice, {soffice, start_link, []}, permanent, 2000, worker, [soffice]},
    Pdf2HtmlEx = {analogy_pdf2htmlex, {pdf2htmlex, start_link, []}, permanent, 2000, worker, [pdf2htmlex]},
    Restart = {one_for_one, 10, 10},
    Children = [SOffice, Pdf2HtmlEx],
    {ok, { Restart, Children} }.

%%====================================================================
%% Internal functions
%%====================================================================

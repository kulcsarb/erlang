-module(soffice).

-behaviour(gen_server).

%% API
-export([start_link/0, to_html/1, to_pdf/1]).

%% GenServer callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).


-define(TMPDIR, "/tmp").

-define(SERVER, analogy_soffice).

%%%=======================================================
%%  API definitions
%%%========================================================
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

to_html(Binary) ->
    gen_server:call(?SERVER, {to_html, Binary}).

to_pdf(Binary) ->
    gen_server:call(?SERVER, {to_pdf, Binary}).


%%%========================================================
%% GENSERVER Callbacks
%%%========================================================
init([]) ->
    io:format("starting ~s  ~p~n", [?SERVER, self()]),
    {ok, {}}.

handle_call(Request, _, State) ->
    Result = case Request of
        {to_html, Binary} -> convert(Binary, "html");
        {to_pdf, Binary} -> convert(Binary, "pdf")
    end,
    {reply, Result, State}.

handle_cast(_, State) ->
    {noreply, State}.

handle_info(_, State) ->
    {noreply, State}.

terminate(Reason, _) ->
    io:format("terminate ~s: ~p", [?SERVER, Reason]).

code_change(_, _, State) ->
    {ok, State}.


%%%========================================================
%% Private functions
%%%========================================================

convert(Binary, ExportPlugin) ->
    Tempfile = random_filename(),
    Ext = get_extension(re:split(ExportPlugin, ":", [{return, list}])),
    InputFile = Tempfile ++ ".bin",
    OutputFile = Tempfile ++ Ext,
    try
        case save_to_temp_file(Binary, InputFile) of
            ok -> call_soffice(InputFile, OutputFile, ExportPlugin);
            {error, Reason} -> {error, filewrite, Reason}
        end
    after
        remove_file(InputFile),
        remove_file(OutputFile)
    end.


call_soffice(InputFile, OutputFile, ExportPlugin) -> 
    _Output = os:cmd(["soffice --headless --convert-to ", ExportPlugin, " --outdir ", ?TMPDIR, " ", InputFile]),
%%    io:format("soffice output: ~s~n", [Output]),
    file:read_file(OutputFile).


get_extension([Ext, _]) ->
    "." ++ Ext;
get_extension([Ext]) ->
    "." ++ Ext.

random_filename() ->
    rand:seed(exs64),
    ["0." ++ Number] = io_lib:fwrite("~p", [rand:uniform()]),
    filename:join([?TMPDIR, Number]).

save_to_temp_file(Binary, Filename) ->
    io:format("saving to ~p~n", [Filename]),
    file:write_file(Filename, Binary).
%%    case ResultFilename, Binary) of
%%        {error, Reason} -> io:format("Error while saving to file ~p: ~p~n", [Filename, Reason]),
%%                           {error, Reason};
%%        ok -> ok
%%    end.

remove_file(Filename) ->
    file:delete(Filename).
%%    case file:delete(Filename) of
%%        ok -> io:format("deleted ~p~n", [Filename]);
%%        {error, Reason} -> io:format("error while deleting ~p: ~p~n", [Filename, Reason])
%%    end.



-module(matchranker).

%%-compile(native).
%%-compile(inline).
%% -compile({inline_size, 1000}).
%% -compile({inline_effort, 2000}).
%% -compile({inline_unroll, 6}).

-behaviour(gen_server).

-define(SERVER, ?MODULE).

%% API
-export([main/0, eval/6, start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).


start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

eval(OccurrencesTable, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword) ->
    gen_server:call(?SERVER, {eval, OccurrencesTable, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword}).


init([]) ->
    {ok, {}}.

handle_call({eval, OccurrencesTable, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword}, _, State) ->
    Result = evaluate(OccurrencesTable, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword),
    {reply, Result, State}.

handle_cast(_, State) ->
    {noreply, State}.

handle_info(_, State) ->
    {noreply, State}.

terminate(_, _) ->
    true.

code_change(_, _, State) ->
    {ok, State}.


%% @TODO move constants to another place and figure out a nicer solution than this

-define(combinations_explode, 400000).
-define(good_enough_match_threshold, 0.38).
-define(penalty_for_reverse_order, -1).
-define(penalty_for_intervening_eos, 1).
-define(penalty_for_intervening_eop, 2).
-define(close_vicinity_distance, 3).
-define(probability_at_border_of_close_vicinity, 0.4).
-define(close_gradient, ((1.0 - ?probability_at_border_of_close_vicinity) / ?close_vicinity_distance)).
-define(neighbourhood_inner_distance, 4).
-define(neighbourhood_outer_distance, 25).
-define(neighbourhood_inner_probability, 0.4).
-define(distant_probability, 0.01).
-define(far_gradient, ((?neighbourhood_inner_probability - ?distant_probability) / (?neighbourhood_outer_distance - ?neighbourhood_inner_distance -1))).
-define(impact_of_surface_form_difference, 0.5).






%% =============================================================
%% @doc Computes the cartesian product of the input array
%% 
%% @spec iter_cartesian(ArrayOfArrays) -> List
%%
%% @end
%% =============================================================

%% @doc Currently unused, as :flatten takes too much time
%cartesian(L) ->
%    lists:map(fun(X) -> lists:flatten(X) end, iter_cartesian(L)).

iter_cartesian([]) -> [];
iter_cartesian([L]) -> L;
iter_cartesian([F, N|T]) ->
    iter_cartesian([ pair_cartesian(F, N) | T]).
pair_cartesian(L1, L2) ->
    [[X, Y] || X <- L1, Y <- L2].

flatpairs([], Acc) -> Acc;
flatpairs([[H|T1]|T2], Acc) ->
    flatpairs([H|T1], [{hd(T2), hd(T1)} | Acc]);
flatpairs([H|T], Acc) ->
    [{hd(T), H} | Acc].


between([], _, _, Acc) -> Acc;
between([H|T], Low, High, Acc) when Low < H, H < High -> between(T, Low, High, Acc+1);
between([_|T], Low, High, Acc) -> between(T, Low, High, Acc).


%%
%Tells how much skip between tokens is allowed to still satisfy the good_enough_match_threshold
%
%This is a reverse calculation of how evaluate_occurrence_pattern_distance works
%max_skip_of_good_matches() -> 5.
max_skip_of_good_matches() ->
    if
        ?good_enough_match_threshold >= ?probability_at_border_of_close_vicinity ->
            % This is somewhat a simplification:
            ?close_vicinity_distance;
        ?good_enough_match_threshold >= ?neighbourhood_inner_probability ->
            ?neighbourhood_inner_distance;
        ?good_enough_match_threshold > ?distant_probability -> 
            trunc(?neighbourhood_inner_distance - ((?good_enough_match_threshold - ?neighbourhood_inner_probability) / ?far_gradient));
        true ->
            10000
    end.

calc_distances([], MinDist, _, _, _, _) -> MinDist;
calc_distances([{Current, Prev} | Tail], MinDist, Eos, Eop, Stopword, Cache) ->
    case ets:lookup(Cache, {Current, Prev}) of
        [{{_, _}, D}] -> Dist = D;
        _ ->
            Dist = distance_of_occurrences(Current, Prev, Eos, Eop, Stopword),
            ets:insert(Cache, {{Current, Prev}, Dist})
    end,
%    Dist = distance_of_occurrences(Current, Prev, Eos, Eop, Stopword),
    case Dist >= ?good_enough_match_threshold of
        true -> calc_distances(Tail, min(MinDist, Dist), Eos, Eop, Stopword, Cache);
        false -> Dist
    end.

evaluate_occurrence_pattern_distance(Occurrences, Eos, Eop, Stopword, Cache) ->
    OccurrencePairs = flatpairs(Occurrences, []),
    calc_distances(OccurrencePairs, 1.0, Eos, Eop, Stopword, Cache).


distance_of_occurrences(Current, Prev, EosPositions, EopPositions, StopwordPositions) ->
    Base = abs(Prev+1-Current),
    Distance = if
        Prev > Current ->
            InterveningEos = between(EosPositions, Current, Prev, 0),
            InterveningEop = between(EopPositions, Current, Prev, 0),
            InterveningStopword = between(StopwordPositions, Current, Prev, 0),
            Base + ?penalty_for_reverse_order + (InterveningEos * ?penalty_for_intervening_eos + InterveningEop * ?penalty_for_intervening_eop - InterveningStopword);
        true -> 
            InterveningEos = between(EosPositions, Prev, Current, 0),
            InterveningEop = between(EopPositions, Prev, Current, 0),
            InterveningStopword = between(StopwordPositions, Prev, Current, 0),
            Base + (InterveningEos * ?penalty_for_intervening_eos + InterveningEop * ?penalty_for_intervening_eop - InterveningStopword)
    end,
    if
        Distance =< ?close_vicinity_distance ->
            1 - (Distance * ?close_gradient);
        Distance =< ?neighbourhood_inner_distance ->
            ?neighbourhood_inner_probability;
        Distance < ?neighbourhood_outer_distance ->
            ?neighbourhood_inner_probability - ((Distance - ?neighbourhood_inner_distance) * ?far_gradient);
        true ->
            ?distant_probability
    end.



create_combinations_full(OccurrencesTable) ->
    iter_cartesian(OccurrencesTable).


create_combinations(OccurrencesTable) when length(OccurrencesTable) == 1 ->
    lists:map(fun(X) -> [X] end, lists:nth(1, OccurrencesTable));
create_combinations([FirstPosList | RestOfOccurrences]) ->
%    io:format("FirstPosList: ~p~n", [FirstPosList]),
%    io:format("RestOfOccurrences: ~p~n", [RestOfOccurrences]),
%    io:format("skip distance: ~p~n", [max_skip_of_good_matches()]),
    create_trimmed_combinations(FirstPosList, RestOfOccurrences, []).

create_trimmed_combinations([], _, Combinations) -> Combinations;
create_trimmed_combinations([StartingPoint|T], RestOfOccurrences, Combinations) ->
    TrimmedOccurrences = trim_occurrences(RestOfOccurrences, StartingPoint),
    if
        length(TrimmedOccurrences) > 0 ->
%            io:format("~p, before: ~p after: ~p ~n ", [StartingPoint, length(RestOfOccurrences), length(TrimmedOccurrences)]),
            SubOccurrences = [[StartingPoint] | lists:reverse(TrimmedOccurrences)],
 %           io:format("generating cartesian for: ~p~n", [SubOccurrences]),
            SubResult = iter_cartesian(SubOccurrences), 
            %%            sub_result = [oc.tolist() for oc in sub_result if len(set(oc)) == oc.size] WTF??
            create_trimmed_combinations(T, RestOfOccurrences, lists:append(Combinations, SubResult));
        true ->
            create_trimmed_combinations(T, RestOfOccurrences, Combinations)
    end.


trim_occurrences(Table, PointOfReference) ->
    trim_occurrences(Table, 0, PointOfReference, []).

trim_occurrences([], _, _, TrimmedTable) -> TrimmedTable;
trim_occurrences([Row|T], Idx, PointOfReference, TrimmedTable) ->
    MinPos = PointOfReference - (Idx + 1) * max_skip_of_good_matches(),
    MaxPos = PointOfReference + (Idx + 1) * max_skip_of_good_matches(),
    Filter = fun(Min, Max) -> fun(X) -> Min =< X andalso X =< Max end end,
    %io:format("~p, ~p : ~p~n", [MinPos, MaxPos, Row]),
    TrimmedRow = lists:filter(Filter(MinPos, MaxPos), Row),
    case length(TrimmedRow) of
        0 -> [];
        _ -> trim_occurrences(T, Idx+1, PointOfReference, [TrimmedRow | TrimmedTable])
    end.




compare_surface_form_of_expressions(OriginalWords, ComparedWords) ->
    compare_surface_form_of_expressions(OriginalWords, ComparedWords, 1.0).

compare_surface_form_of_expressions([], [], MatchValue) -> MatchValue;
compare_surface_form_of_expressions([Ow | OriginalWords], [Cw | ComparedWords], MatchValue) ->
    Substr = lcs:lcs(string:to_lower(Ow), string:to_lower(Cw)),
    WordMatchValue = length(Substr) / lists:max([length(Ow), length(Cw)]),
    DampenedWordMatchValue = (1 - (1 - WordMatchValue) * ?impact_of_surface_form_difference),
    compare_surface_form_of_expressions(OriginalWords, ComparedWords, MatchValue * DampenedWordMatchValue).



evaluate_pattern_of_keyword_in_text(OccurrencesTable, Eos, Eop, Stopword) ->
%    io:format("Occurrences: ~p~n", [OccurrencesTable]),
    Combinations = create_combinations(OccurrencesTable),

    %must be 0.4:
    %Combinations = [ [[[[[[[[[[[[[[[3331,3332],3333],3334],3335],3336],3337],3338],3339],3340],3341],3349],3343],3344],3345],3346] ],
    % must be 1.0:
    %Combinations = [ [[[[[[[[[[[[[[[3331,3332],3333],3334],3335],3336],3337],3338],3339],3340],3341],3342],3343],3344],3345],3346] ],

    io:format("combinations: ~p~n", [length(Combinations)]),

    Cache = ets:new(matchranker_cache, [set]),
    Relevances = lists:map(fun(C) -> {evaluate_occurrence_pattern_distance(C, Eos, Eop, Stopword, Cache), C} end,  Combinations),

    %% io:format("~p~n", [ets:info(Cache)]),
    ets:delete(Cache),
    FilteredRelevances = lists:filter(fun({R, _}) -> R > ?good_enough_match_threshold end, Relevances),
    lists:map(fun({R, L}) -> {R, lists:flatten(L)} end, FilteredRelevances).


evaluate_surface_form_difference(KeywordSurfaceForms, TextSurfaceForms, Matches) ->
    ComputeSurfaceFormDifference = fun({Relevance, Positions}) ->
                                           WordsInText = [maps:get(P, TextSurfaceForms) || P <- Positions],
                                           SurfaceFormMatch = compare_surface_form_of_expressions(WordsInText, maps:values(KeywordSurfaceForms)),
                                           {Relevance * SurfaceFormMatch, Positions}
                                   end,
    lists:map(ComputeSurfaceFormDifference, Matches).


evaluate(OccurrencesTable, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword) ->
    PositionBasedMatches = evaluate_pattern_of_keyword_in_text(OccurrencesTable, Eos, Eop, Stopword),
    PositionAndSurfaceFormMatches = evaluate_surface_form_difference(KeywordSurfaceForms, TextSurfaceForms, PositionBasedMatches),
    lists:sort(fun({R1, _}, {R2, _}) -> R1 >= R2 end, PositionAndSurfaceFormMatches).


main() ->
    {ok,Value} = file:script("match2.txt"),
    {Occurrences, KeywordSurfaceForms, TextSurfaceForms, Eos, Eop, Stopword} = Value,
    % {Occurrences, Eos, Eop, Stopword} = Value,
    Start = os:system_time(millisecond),
    Result = evaluate(Occurrences, KeywordSurfaceForms, TextSurfaceForms,  Eos, Eop, Stopword),
    End = os:system_time(millisecond),
    io:format("time elapse: ~f sec ~n", [(End-Start) / 1000.0]),
    io:format("results: ~p~n", [length(Result)]),
    if
        length(Result) > 0 ->
            hd(Result);
        true -> []
    end.


-module(fix).

-behaviour(gen_server).

-export([start_link/0, logon/3, encode/1, decode/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-define(FIELD_SEPARATOR, "=").
-define(TAG_SEPARATOR, "|").

-record(session, {username, password, msgseqnum, lastsenttime, lastreceivedtime}).
-record(logon, {username, password, heartbtint=30, resetseqnumflag="Y", encryptmethod="N"}).
-record(message, {sendercompid, targetcompid, msgseqnum, payload}).


-define(MSG_TYPES, [{heartbeat, "0"}, 
                    {test_request, "1"},
                    {resend_request, "2"},
                    {reject, "3"},
                    {sequence_reset, "4"},
                    {logout, "5"}]).
-define(MSGTYPE, 35).
-define(USERNAME, 553).
-define(PASSWORD, 554).
-define(RESETSEQNUMFLAG, 141).
-define(HEARTBTINT, 108).
-define(ENCRYPTMETHOD, 98).


start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).


logon(Username, Password, HeartBtInt) ->
    #logon{username=Username, password=Password, heartbtint=HeartBtInt}.


record_to_list(Msg) ->
    [Type|Values] = tuple_to_list(Msg),
    lists:zipwith(fun(F, V) -> [F, V] end, record_fields(Type), Values).


%add_session_info(SenderCompId, TargetCompId, MsgSeqNum, Payload) ->
%    #message{sendercompid=SenderCompId, targetcompid=TargetCompId, msgseqnum=MsgSeqNum, payload=Payload}.

add_session_info(SenderCompId, TargetCompId, MsgSeqNum, Payload) ->
    [[sendercompid, SenderCompId] | Payload]


encode(Msg) ->
    Message = record_to_list(Msg),
    io:format("~p~n", [Message]),
    list_to_binary(
      lists:concat(
        lists:join(?TAG_SEPARATOR,
                 lists:map(
                   fun([Field, Value]) -> [integer_to_list(Field), ?FIELD_SEPARATOR, Value] end,
                   lists:map(fun([F, V]) -> translate(F, V) end, Message)
                  )
                ))).

add_header(Message, Type) ->
    Message.

add_footer(Message) ->
    Message.


decode(Bin) ->
    Tokens = string:tokens(binary_to_list(Bin), "|="),
    Message = decode(Tokens, []),
    {_, MsgType} = lists:keyfind(msgtype, 1, Message),
    record_from_list(MsgType, Message).


decode([], Acc) -> Acc;
decode([Token,Value|Rest], Acc) ->
    decode(Rest, [ translate(list_to_integer(Token), Value) | Acc]).


record_from_list(Type, Msg) ->
    Values = lists:map(fun(F) -> {_, V} = lists:keyfind(F, 1, Msg), V end, record_fields(Type)),
    list_to_tuple([Type | Values]).


translate(Field, V) when is_integer(Field) ->
    case Field of
        ?MSGTYPE -> {msgtype, msgtype(V)};
        ?USERNAME -> {username, V};
        ?PASSWORD -> {password, V};
        ?RESETSEQNUMFLAG -> {resetseqnumflag, V};
        ?HEARTBTINT -> {heartbtint, list_to_integer(V)};
        ?ENCRYPTMETHOD -> {encryptmethod, V}
    end;

translate(Field, V) when is_atom(Field) ->
    case Field of
        msgtype -> [?MSGTYPE, msgtype(V)];
        username -> [?USERNAME, V];
        password -> [?PASSWORD, V];
        resetseqnumflag -> [?RESETSEQNUMFLAG, V];
        heartbtint -> [?HEARTBTINT, integer_to_list(V)];
        encryptmethod -> [?ENCRYPTMETHOD, V]
    end.


%% a slightly slower solution:
%% msgtype(Type) when is_atom(Type) ->
%%     {_, V} = lists:keyfind(Type, 1, ?MSG_TYPES),
%%     V;
%% msgtype(Type) when is_list(Type) ->
%%     {F, _} = lists:keyfind(Type, 2, ?MSG_TYPES),
%%     F.

msgtype(Type) when is_atom(Type) ->
    case Type of
        heartbeat -> "0";
        test_request -> "1";
        resend_request -> "2";
        reject -> "3";
        sequence_reset -> "4";
        logout -> "5";
        logon -> "A"
    end;

msgtype(Type) when is_list(Type) ->
    case Type of
        "0" -> heartbeat;
        "1" -> test_request;
        "2" -> resend_request;
        "3" -> reject;
        "4" -> sequence_reset;
        "5" -> logout;
        "6" -> logon
    end.


record_fields(Type) ->
    case Type of
        logon -> record_info(fields, logon);
        _ -> []
    end.




%% ===================================================================================
%%
%%
%%
%% ====================================================================================

init([]) ->
    {ok, #session{}}.


handle_call({login, {Username, Password}}, _, State) ->
    NewState = State#session{username=Username, password=Password},
    {reply, {}, NewState};

handle_call(Request, From, State) ->
    {noreply, State}.


handle_cast(Request, State) ->
    {noreply, State}.


handle_info(_, State) ->
    {noreply, State}.

terminate(_, _) ->
    ok.

code_change(_, _, State) ->
    {ok, State}.




-module(counter).

-behaviour(gen_server).

-compile([
		{nowarn_unused_function, [get_value/1]}		
		]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([start/0, start_link/0, stop/0, inc/0, dec/0, value/0, logs/0, update/0, fuck/0]).

-define(SERVER, ?MODULE).


% -record(event, {
% 				op		:: atom(),
% 				node	:: atom(),
% 				vsn		:: integer()
% 				}).

-record(counter, {
				node	:: atom(),
				inc = 0 :: integer(),
				dec = 0	:: integer()
				}).


%%% API


start_link() ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).


start() ->
	gen_server:start({local, ?SERVER}, ?MODULE, [], []).


stop() ->
	gen_server:stop(?SERVER).


fuck() ->
	gen_server:cast(?SERVER, fuck).

inc() ->
	gen_server:cast(?SERVER, inc).

dec() ->
	gen_server:cast(?SERVER, dec).

value() ->
	gen_server:call(?SERVER, get_value).


logs() ->
	gen_server:call(?SERVER, get_state).

update() ->
	gen_server:cast(?SERVER, get_updates).


%%% GenServer functions

-spec init(_Arg::any()) -> tuple().
init(_Arg) ->	
	io:format("Counter ~p staring ~n", [self()]),
	process_flag(trap_exit, true),
	net_kernel:monitor_nodes(true),
	{ok, #{node() => #counter{node=node()} }, 0}.


handle_call(get_value, _From, State) ->
	{reply, get_value(State), State};

handle_call(get_state, _From, State) ->
	{reply, State, State}.


handle_cast(inc, State) ->
	NewState = maps:update_with(node(), fun increase_counter/1, State),	
	send_state(NewState),
	{noreply, NewState};

handle_cast(dec, State) ->
	NewState = maps:update_with(node(), fun decrease_counter/1, State),		
	send_state(NewState),
	{noreply, NewState};

handle_cast({update, OtherState}, State) ->
	io:format("Received update: ~p~n", [OtherState]),		
	NewState = merge_states(OtherState, State),	
	{noreply, NewState};


handle_cast({sync, OtherState, From}, State) ->	
	io:format("Received sync: ~p~n", [OtherState]),		
	NewState = merge_states(OtherState, State),
	send_state(State, [From]),
	{noreply, NewState};


handle_cast(fuck, State) ->
	io:format("Clusterfuck detected, stopping!"),
	{stop, clusterfuck, State}.


handle_info(timeout, State) ->
	sync_state(State),
	{noreply, State};


handle_info({nodeup, Node}, State) ->
	io:format("nodeup ~p~n", [Node]),
	send_state(State, [Node]),
	{noreply, State};


handle_info({nodedown, Node}, State) ->
	io:format("nodedown ~p~n", [Node]),
	{noreply, State}.


%handle_info(_, State) ->
%	{noreply, State}.


terminate(Reason, State) ->
	io:format("terminate:  ~p ~p~n", [Reason, State]).

code_change(_, _, _) ->
	ok.


%%% Private functions
increase_counter(C) when is_record(C, counter) ->
	C#counter{inc = C#counter.inc + 1}.

decrease_counter(C) when is_record(C, counter) ->
	C#counter{dec = C#counter.dec + 1}.


get_value(State) ->
	lists:sum(
		lists:map(
			fun(S) -> S#counter.inc - S#counter.dec end,
			maps:values(State)
		)
	).



send_state(State) -> 
	send_state(State, nodes()).

send_state(State, Nodes) when is_list(Nodes) ->
	io:format("~p -update-> ~p~n", [node(), Nodes]),
	gen_server:abcast(Nodes, ?SERVER, {update, State}).

sync_state(State) ->
	sync_state(State, nodes()).

sync_state(State, Nodes) when is_list(Nodes) ->
	io:format("~p -sync-> ~p~n", [node(), Nodes]),
	gen_server:abcast(Nodes, ?SERVER, {sync, State, node()}).


merge_states(State, Other) when is_map(Other) ->
	merge_states(State, maps:to_list(Other));

merge_states(State, []) -> State;

merge_states(State, [{K, C}| T]) ->
	case maps:get(K, State, false) of 
		false -> 
			NewState = maps:put(K, C, State);
		Counter -> 			
			NewState = maps:update(K, merge_counter(Counter, C), State)
	end,
	merge_states(NewState, T).


merge_counter(A, B) when A#counter.node == B#counter.node ->
	#counter{
			node = A#counter.node,
			inc = max(A#counter.inc, B#counter.inc), 
			dec = max(A#counter.dec, B#counter.dec)
			}.





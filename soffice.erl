-module(soffice).

-export([to_html/1, to_pdf/1]).

-define(TMPDIR, "/tmp").


to_html(Binary) ->
    convert(Binary, "html").

to_pdf(Binary) ->
    convert(Binary, "pdf").


convert(Binary, ExportPlugin) ->
    Tempfile = random_filename(),
    Ext = get_extension(re:split(ExportPlugin, ":", [{return, list}])),
    InputFile = Tempfile ++ ".bin",
    OutputFile = Tempfile ++ Ext,
    io:format("convert: ~s ~s ~s~n", [InputFile, OutputFile, ExportPlugin]),
    save_to_temp_file(Binary, InputFile),
    Content = call_soffice(InputFile, OutputFile, ExportPlugin),
    remove_file(InputFile),
    remove_file(OutputFile),
    Content.


call_soffice(InputFile, OutputFile, ExportPlugin) -> 
    Output = os:cmd(["soffice --headless --convert-to ", ExportPlugin, " --outdir ", ?TMPDIR, " ", InputFile]),
    io:format("soffice output: ~p~n", [Output]),
    {ok, Content} = file:read_file(OutputFile),
    Content.


get_extension([Ext, _]) ->
    "." ++ Ext;
get_extension([Ext]) ->
    "." ++ Ext.

random_filename() ->
    rand:seed(exs64),
    ["0." ++ Number] = io_lib:fwrite("~p", [rand:uniform()]),
    filename:join([?TMPDIR, Number]).

save_to_temp_file(Binary, Filename) ->
    io:format("saving to ~p~n", [Filename]),
    ok = file:write_file(Filename, Binary).

remove_file(Filename) ->
    io:format("deleting ~p~n", [Filename]),
    ok = file:delete(Filename).


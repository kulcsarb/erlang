-module(dubai).
-export([add/1, add/0, check/2, fact/1]).

-lofasz(eeeeeh).
-name(balage).


%%% simple adder functions to practice 
add() -> 0.

add(X) when X == 4 -> 4;  % yeah, it's amazing.
add(X) -> X + 2.   % I can add 2, too. 

state(0) -> zero;
state(X) when X > 0 -> positive;
state(_) -> negative.


check(S, X) -> 
    % case of to check the outcome of a single expression
    _State = case S of 
        0 -> zero;
        _ when S > 0 -> positive;
        _ when S < 0 -> negative
    end,

    % if is used to check on any expression
    Result = if 
        S == 0 -> 'fine';
        X == 0 -> 'efff';
        (S == 3) or (X == 4) -> 2;
        true -> 'final'
    end,

    {state(S), Result}.


fact(X) when X > 1 -> X * fact(X-1);
fact(_) -> 1.




